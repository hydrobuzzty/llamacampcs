﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogDisplay : MonoBehaviour
{
    public Conversation conversation;

    public GameObject speakerLeft;
    public GameObject speakerRight;
    public GameObject speakerLeftTwo;
    public GameObject speakerRightTwo;
    public GameObject SwimmingCollider;
    private SpeakerUI speakerUILeft;
    private SpeakerUI speakerUILeftTwo;
    private SpeakerUI speakerUIRight;
    private SpeakerUI speakerUIRightTwo;
    private bool swimmingColliderActive = false;
    private bool entryDialogueColliderActive = false;
    public GameObject pressEText;

    private int activeLineIndex = 0;

    void Start()
    {
        speakerUILeft = speakerLeft.GetComponent<SpeakerUI>();
        speakerUILeftTwo = speakerLeftTwo.GetComponent<SpeakerUI>();
        speakerUIRight = speakerRight.GetComponent<SpeakerUI>();
        speakerUIRightTwo = speakerRightTwo.GetComponent<SpeakerUI>();

        speakerUILeft.Speaker = conversation.speakerLeft;
        speakerUILeftTwo.Speaker = conversation.speakerLeftTwo;
        speakerUIRight.Speaker = conversation.speakerRight;
        speakerUIRightTwo.Speaker = conversation.speakerRightTwo;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "ColliderSwimming")
        {
            swimmingColliderActive = true;
        }

        if (other.tag == "EntryDialogueCollider")
        {
            entryDialogueColliderActive = true;
        }
    }

    private void Update()
    {
        if (swimmingColliderActive == true && Input.GetKeyDown(KeyCode.E))
        {
            AdvanceConversationSwimming();
        }

        if (entryDialogueColliderActive == true && Input.GetKeyDown(KeyCode.E))
        {
            pressEText.SetActive(false);
            AdvanceConversationAfterSwimming();
        }
    }

    void AdvanceConversationSwimming()
    {
        if (activeLineIndex < conversation.lines.Length)
        {
            DisplayLine();
            activeLineIndex += 1;
        }
        else
        {
            speakerUILeft.Hide();
            speakerUILeftTwo.Hide();
            speakerUIRight.Hide();
            speakerUIRightTwo.Hide();
            activeLineIndex = 0;
            swimmingColliderActive = false;
            SceneManager.LoadScene("Schwimmen");
        }
    }
    
    void AdvanceConversationAfterSwimming()
    {
        if (activeLineIndex < conversation.lines.Length)
        {
            DisplayLine();
            activeLineIndex += 1;
        }
        else
        {
            speakerUILeft.Hide();
            speakerUILeftTwo.Hide();
            speakerUIRight.Hide();
            speakerUIRightTwo.Hide();
            activeLineIndex = 0; 
            entryDialogueColliderActive = false;
        }
    }

    void DisplayLine()
    {
        Line line = conversation.lines[activeLineIndex];
        Character character = line.character;

        if (speakerUILeft.SpeakerIs(character))
        {
            SetDialog(speakerUILeft, speakerUIRight, speakerUIRightTwo, speakerUILeftTwo, line.text);
        }

        if (speakerUIRight.SpeakerIs(character))
        {
            SetDialog(speakerUIRight, speakerUILeft, speakerUILeftTwo, speakerUIRightTwo, line.text);
        }

        if (speakerUILeftTwo.SpeakerIs(character))
        {
            SetDialog(speakerUILeftTwo, speakerUIRight, speakerUIRightTwo, speakerUILeft, line.text);
        }

        if (speakerUIRightTwo.SpeakerIs(character))
        {
            SetDialog(speakerUIRightTwo, speakerUIRight, speakerUILeft, speakerUILeftTwo, line.text);
        }
    }

    public void SetDialog(
        SpeakerUI activeSpeakerUI, 
        SpeakerUI inactiveSpeakerUI, 
        SpeakerUI inactiveSpeakerUITwo,
        SpeakerUI inactiveSpeakerUIThree,
        string text
        )
    {
        activeSpeakerUI.Dialog = text;
        activeSpeakerUI.Show();
        inactiveSpeakerUI.Hide();
        inactiveSpeakerUITwo.Hide();
        inactiveSpeakerUIThree.Hide();
    }
}
