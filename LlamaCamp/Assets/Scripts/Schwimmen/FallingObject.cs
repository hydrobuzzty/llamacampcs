using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using Random = UnityEngine.Random;


    public class FallingObject : MonoBehaviour
    {
        
        /// <summary>
        /// Serialized value of the min speed the falling object should have, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private float minSpeed;

        /// <summary>
        /// Serialized value of the max speed the falling object may have, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private float maxSpeed;
        
        /// <summary>
        /// Internally stored value for the movement speed
        /// </summary>
        private float speed;

        private void Awake()
        {
            // When a falling object gets spawned we set the speed value randomly between the
            // minSpeed and maxSpeed values
            speed = Random.Range(minSpeed, maxSpeed);
        }

        private void Update()
        {
            // The falling object goes straight down depending on its' speed value
            transform.position += Vector3.down * speed * Time.deltaTime;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            
            // When we collide with something that has the DEATHZONE Tag
            if (other.CompareTag(GameTags.DEATHZONE))
            {
                ProcessFallingObject(gameObject);
            }
        }
        public void ProcessFallingObject(GameObject fallingObject)
        {
            // After the tween is complete we destroy the falling object
            Destroy(fallingObject);
        }
    }