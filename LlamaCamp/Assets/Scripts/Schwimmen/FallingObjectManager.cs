using UnityEngine;


    public class FallingObjectManager : MonoBehaviour
    {
        
        /// <summary>
        /// Serialized array of transforms, the array elements are our spawnPoints, assign them in the Inspector
        /// </summary>
        [SerializeField]
        private Transform[] spawnPoints;

        /// <summary>
        /// Serialized array of GameObjects/Prefabs, the array elements are our falling Object Prefabs
        /// assign them in the Inspector
        /// </summary>
        [SerializeField]
        private GameObject[] fallingObjectPrefabs;

        /// <summary>
        /// Serialized value of the time window for spawning a falling object, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private float spawnTime;

        /// <summary>
        /// Internally stored value of the timer for spawning which ticks down
        /// </summary>
        private float timer;

        private void Start()
        {
            // We set the timer to our spawnTime value
            timer = spawnTime;

            // Spawning the first falling object
            SpawnFallingObject();
        }

        private void Update()
        {
            // We tick down our timer, if it's below 0 we reset the timer and spawn a falling object
            timer -= Time.deltaTime;
            if (timer <= 0f)
            {
                timer = spawnTime;
                SpawnFallingObject();
            }
        }

        /// <summary>
        /// Randomly spawns a falling Object at a random position
        /// </summary>
        private void SpawnFallingObject()
        {
            // We randomly store a number between 0 and the length of our fallingObjectPrefabs-Array in a local int variable
            // since we do want to choose a random element from our array which we want to spawn
            int randomFallingObjectIndex = Random.Range(0, fallingObjectPrefabs.Length);

            // Then we access the element of the array with that randomly set index variable
            // and spawn an instance of the Prefab and store its value to a local GameObject variable
            GameObject newFallingObject = Instantiate(fallingObjectPrefabs[randomFallingObjectIndex]);

            // After that we repeat the same thing for our spawnPoints-Array!
            // We randomly store a number between 0 and the length of our spawnPoints-Array in a local int variable
            // since we do want to choose a random element from our array which we want to position the spawned fallingObject
            int randomSpawnPointIndex = Random.Range(0, spawnPoints.Length);

            // Then we access the element of the array with that randomly set index variable
            // and set the position of our previously spawned fallingObject to the position of the randomly selected spawnPoint
            newFallingObject.transform.position = spawnPoints[randomSpawnPointIndex].localPosition;
        }

       
        public void ProcessFallingObject(GameObject fallingObject)
        {
            // After the tween is complete we destroy the falling object
            Destroy(fallingObject, 0.11f);
        }
    }
