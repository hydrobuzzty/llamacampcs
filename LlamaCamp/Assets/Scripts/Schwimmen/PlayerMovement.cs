using UnityEngine;


    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField]
        private GameObject canvas;   
        /// <summary>
        /// Serialized value of the movement speed, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private float speed;

        /// <summary>
        /// Serialized value of the left edge, this is the furthest the player may move to the left
        /// assign this in the Inspector
        /// </summary>
        [SerializeField]
        private float leftEdge;

        /// <summary>
        /// Serialized value of the right edge, this is the furthest the player may move to the right
        /// assign this in the Inspector
        /// </summary>
        [SerializeField]
        private float rightEdge;
        
        private void Update()
        {
            // If the player hits A or the Left Arrow on the keyboard we move to the left
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                // Move left
                transform.position += Vector3.left * speed * Time.deltaTime;
            }

            // If the player hits D or the Right Arrow on the keyboard we move to the right
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                // Move right
                transform.position += Vector3.right * speed * Time.deltaTime;
            }

            // Since we do not want to go beyond the scene/camera bounds, we force the position on the x-axis of our player
            // We accomplish this by clamping the position:

            // Transform.position is a so called "Struct" (Google that!)
            // And C# has a very specific way of accessing and handling these structs
            // We can't set the position.x property , we have to work with a temporary variable as a workaround
            // So we cache the current position
            Vector3 cachedPosition = transform.position;

            // Then we perform the clamping operation on the .x value of the cached Position
            // with the Mathf.Clamp() Method and pass in the value which has to be clamped, in our case
            // the cachedPosition.x, we clamp it between the leftEdge and the rightEdge
            // the value which gets returned will be set to the cachedPosition.x value
            cachedPosition.x = Mathf.Clamp(cachedPosition.x, leftEdge, rightEdge);

            // At last we set the transform.position to the cachedPosition Vector with it's clamped x-Value
            transform.position = cachedPosition;
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            // When we collide with something that has the PLAYER Tag
            if (other.CompareTag(GameTags.ENEMY))
            {
                canvas.SetActive(true);
                Time.timeScale = 0;
            }
        }   
    }
